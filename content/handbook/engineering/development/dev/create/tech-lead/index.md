---
title: "Create Stage: Tech Leads"
description: The objective of this page is to provide a clear documentation of the responsibilities and attributes associated with the role of a tech lead within the Create Stage.
---

## What is a Tech Lead
This role is outlined [here](https://handbook.gitlab.com/handbook/engineering/ic-leadership/tech-lead/#the-tech-lead-role).

## How is a Tech Lead different from a Domain Expert

Tech Leads and Domain Experts share similarities and differences. The tables below clarify distinctions between the two roles.

### Similarities
| Criteria                      | Tech Lead                                     | Domain Expert                                |
| ----------------------------- | --------------------------------------------- | -------------------------------------------- |
| **Expertise Requirement**      | Requires domain knowledge and expertise       | Requires substantial experience in a specific area |
| **Collaboration**              | Collaboration with Engineering Managers and Product Managers | Collaboration with team members and stakeholders |
| **Mentoring**                 | Provides technical guidance and mentoring     | Mentors team members in their specific area   |
| **Not a Managerial Role**      | Not a manager                                  | Not a manager                                 |

### Differences
| Criteria                          | Tech Lead                                     | Domain Expert                                |
|-----------------------------------|-----------------------------------------------|----------------------------------------------|
| **Nature of Role**                | Temporary role tied to a specific topic/project | Ongoing role with substantial expertise     |
| **Assignment Criteria**           | Efficiency, domain knowledge, expertise        | Substantial experience in specific areas     |
| **Seniority**                     | Any engineer, regardless of seniority          | Encouraged for team members with experience |
| **Providing Technical Vision**    | Providing technical vision and architecture   |  Not required. |
| **Guidance and Mentoring**        | Offering technical guidance and mentoring     | Conducting code and MR reviews in their domain |
| **Planning and Prioritizing Work**| Planning and prioritizing work                 | Not required           |
| **Tracking Progress and Reporting**| Tracking progress on commitments and reporting | Ensuring progress tracking and reporting within their domain, providing insights into achievements, challenges, and goals |
| **Risk Management**               | Identifying, assessing, and managing technical risks that may impact deliverables | Managing technical risks associated with specific technology, product feature, or codebase area |
| **Coordination**                  | Overseeing the work of others and helping remove blockers | Coordinating with team members in their domain, ensuring smooth collaboration and addressing challenges as they arise |
| **Collaboration**                 | Slack channel for collaboration (#tech_leads or topic/project specific channel)  | Not required. |
| **Project Template**              | Utilizes a project template for guidance       | Not required.    |
| **Scope**                         | Tied to a specific topic/project              | Generally encompasses specific technology, feature, or codebase area                                                      |
## Responsibilities
The Tech Lead responsibilites are outlined [here](https://handbook.gitlab.com/handbook/engineering/ic-leadership/tech-lead/#responsibilities-of-a-tech-lead).

## Process


### Determine if the Project needs a Tech Lead

In some projects, having a Tech Lead depends on the team's available capacity. Engineering Managers decide based on the team's Tech Leadership capacity. Use these questions to help evaluate if your project needs a Tech Lead.
1. Is the project technically complex? Does it require a blueprint?
1. Does it involve multiple technologies or integrations?
1. Does the project demand unique technical expertise?
1. Does the project have a high level of risk associated with technical aspects?
1. Is the success of the project critical to the overall business goals?
1. Are there complex technical decisions to be made during the project?
1. Does the project involve architectural choices that need experienced guidance?
1. Is there a tight deadline for project completion?
1. Is there a need for effective communication between technical and non-technical team members?

### Appointing a Tech Lead for a Project

| Step | Engineering Manager's Responsibilities |
|------|---------------------------------------|
| 1    | Assess project needs                  |
| 2    | Evaluate Tech Leadership capacity     |
| 3    | Select a Tech Lead         |


### Project Handbook Page 

All Projects should use the same Project Template.  By adopting this template, the project team can benefit from streamlined processes, enhanced communication, and a proactive approach to project management, ultimately contributing to project success.

## Template

#### Project Overview
* Description: Clearly articulates the project's purpose, goals, and importance.
* Stakeholders: Identifies key stakeholders and their roles, fostering understanding and alignment.
#### Project Objectives
SMART Objectives: Defines Specific, Measurable, Achievable, Relevant, and Time-bound project objectives for focused and attainable goals.
#### Project Team
Roles and Responsibilities: Lists team members along with their specific roles and responsibilities, establishing accountability and coordination.
#### Key Milestones
Measurable Outcomes: Specifies measurable outcomes and success criteria, facilitating clear progress tracking.
Epics with Issues: Utilizes Epics and Issue Boards for structured project organization.
#### Timeline
Phased Milestones: Includes start and end dates for each major phase milestone, ensuring timeline adherence.
#### Risk Management
Identification and Mitigation: Identifies potential risks and strategies for mitigation, preparing the team for unforeseen challenges.
#### Communication Plan
Effective Communication Channels: Outlines communication channels within the team and to stakeholders, enhancing collaboration.
Metrics/Dashboards/Charts: Incorporates visual tools for better project insight.
Labels: Establishes a labeling system for improved organization and tracking.
#### Quality, Security, SRE, Documentation
Epics: Creates separate Epics for Quality, Security, SRE, and Documentation, ensuring focused collaboration in each area.
#### Feedback
Feedback Gathering: Sets up an Epic for both internal and external feedback, promoting continuous improvement.
#### Project Team Meetings
Regular Meetings: Recommends scheduling regular project team meetings to maintain alignment and coordination.
#### Status Report Issue
* Continuous Reporting: Implements a  status report issue format for transparent reporting at a cadence that aligns with the project dynamics.
* Achievements, Challenges, Goals, Lessons: Systematically captures  accomplishments, challenges, upcoming goals, and lessons learned, fostering accountability and continuous improvement.


## Benefits of Using This Template:
1. Clarity and Alignment: Clearly defines project goals, roles, and timelines, ensuring everyone is on the same page.
1. Efficient Communication: Establishes effective communication channels and tools, reducing misunderstandings and improving collaboration.
1. Risk Mitigation: Proactively identifies and addresses potential risks, minimizing project disruptions.
1. Organized Project Structure: Utilizes Epics, Issues, and Labels for a structured and organized project workflow.
1. Continuous Improvement: Encourages feedback gathering and lesson sharing for ongoing learning and enhancement.
1. Transparent Reporting: Weekly status reports provide transparency on achievements, challenges, and upcoming goals.
1. Focused Collaboration: Epics for different project aspects enable focused collaboration in quality, security, SRE, and documentation.